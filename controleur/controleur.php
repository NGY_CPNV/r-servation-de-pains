<?php
if(!isset($_SESSION))
{
    session_start();
}
/**
 * User: Théo.Mauron
 * Date: 18.02.2019
 * Time: 14:51
 */

require_once "modele/modele.php";

//redirection de page
function vue_accueil()
{
    require_once "vue/vue_accueil.php";
}
function vue_produits()
{
    require_once "vue/vue_produits.php";
}
function vue_clients()
{
    require_once "vue/vue_clients.php";
}
function vue_contact()
{
    require_once "vue/vue_contact.php";
}
function vue_login()
{
    require_once "vue/vue_login.php";
}
function vue_inscription()
{
    require_once "vue/vue_inscription.php";
}
function vue_erreur()
{
    require_once "vue/vue_erreur.php";
}


//vers le modele.php
function connexion()
{
    LoginDB($_POST['Login'], $_POST['Password']);
}
function deconnexion()
{
    unset($_SESSION['UserLogin']);
    require_once "vue/vue_accueil.php";
}
function inscription()
{
    //on passe en argument les $_POST qu'on à recupéré au préalable sur le formulaire
    insert_inscription($_POST['Nom'],$_POST['Prenom'],$_POST['Email'],$_POST['Adresse'],$_POST['Login'],$_POST['Password']);
    require_once "vue/vue_accueil.php";
}
?>