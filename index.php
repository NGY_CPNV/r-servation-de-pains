<?php
if(!isset($_SESSION))
{
    session_start();
}
/**
 * Created by PhpStorm.
 * User: Théo Mauron
 * Date: 18.02.2019
 * Time: 14:51
 */
require_once "controleur/controleur.php";
try{
    if (isset($_GET['action']))
    {
        $action = @$_GET['action'];
        switch ($action)
        {
            case 'accueil' :
                vue_accueil();
                break;
                
            case 'produits' :
                vue_produits();
                break;
                
            case 'clients' :
                vue_clients();
                break;
                
            case 'contact' :
                vue_contact();
                break;
                
            case 'login' :
                vue_login();
                break;  
                
            case 'inscription' :
                vue_inscription();
                break;
                
            case 'connexion' :
                connexion();
                break; 
                
            case 'deconnexion' :
                deconnexion();
                break;
                

            case 'demande_inscription' :
                inscription();
                break;

                
            default :
                throw new Exception("Action non valide");
                break;

        }
    }
    else
        vue_accueil();
}

catch (Exception $e)
{
    $_SESSION['erreur']=$e->getMessage();
    vue_erreur();
}


?>