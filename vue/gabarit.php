<?php
if(!isset($_SESSION)){
    session_start();
}

/**
 * User: Theo.MAURON
 * Date: 19.02.2019
 * Time: 08:21
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La Brioche</title>

    
    <link href="contenu/css/gabarit.css" rel="stylesheet">
    <link href="contenu/css/produits.css" rel="stylesheet">
    <link href="contenu/css/contact.css" rel="stylesheet">
    <link href="contenu/css/login.css" rel="stylesheet">
    <link href="contenu/css/inscription.css" rel="stylesheet">
    <!-- fav and touch icons -->
    <link rel="shortcut icon" type="image/png" href="contenu\images\ico\logo_pain.png">

</head>
<body>

    <div id="Titre" class="Texte" style="text-align:right;">
        <div style="float:left; width:1200px; ">
            La Brioche
        </div>
        <div style="width:400px;float:left; height:150px; font-size:24px">
        <?php if(empty($_SESSION['UserLogin'])) : ?>
            <a href="index.php?action=login">
                <button id="" class="Texte btn">Login
                </button>
            </a>
        <?php else: ?>
            <div style="font-size:24px;">
                Bienvenu <?= $_SESSION['UserLogin'] ?>
            </div>
            <a href="index.php?action=deconnexion">
                <button id="" class="Texte btn2">Se déconnecter
                </button>
            </a>
        <?php endif; ?>
        </div>
    </div>
    
    <a href="index.php?action=accueil">
        <button class="Texte menu " style="margin-left:300px; width:260px">Accueil
        </button>
    </a>
    <a href="index.php?action=produits">
        <button class="Texte menu ">
            Produits
        </button>
    </a>
    <a href="index.php?action=clients">
        <button class="Texte menu ">
            Clients
        </button>
    </a>
    <a href="index.php?action=contact">
    <button class="Texte menu ">
        Contact
    </button>
    </a>
   
    <div style="font-size:40px;">
        <form action="#" method="POST">
            <button style="background-color: inherit;border:inherit;float:left;"><img src="contenu/images/ico/loupe.png" style="height:25px; width:25px; margin-top:20px">
            </button>
            <div style="float:left;float:left;width:225px;">
                <input type="text" name="fname" class="Texte" style="background-color:transparent;border:0px;width:225px;font-size:25px">
                <hr style="margin-top:0px;width:208px">
            </div>    
        </form>
    </div>
    <hr style="width:1300px;">
    
    <!-- Contenu -->
<div id="contenu">
    <?=$contenu; ?>

    <!-- Fin Contenu -->
</div>

</body>
<footer>
 <hr style="width:1300px;">
<div style="margin-left:300px; float:left;">
    <img src="contenu/images/ico/copyright.png" style="height:30px; width:85px;">
</div>
<div  style="margin-left:1080px;float:left" >
    <a href="https://www.facebook.com">
        <img class="image" src="contenu/images/ico/facebook.png" >
    </a>
</div>
<div style="margin-left:10px;float:left" >
    <a href="https://www.instagram.com">
        <img class="image" src="contenu/images/ico/instagram.png">
    </a>
</div>
<div style="margin-left:10px;float:left" >
    <a href="https://twitter.com">
        <img class="image" src="contenu/images/ico/twitter.png">
    </a>
</div>
</footer>

<script src="contenu/js/jquery.js" type="text/javascript"></script>
<script src="contenu/js/bootstrap.min.js" type="text/javascript"></script>
<script src="contenu/js/google-code-prettify/prettify.js"></script>

<script src="contenu/js/bootshop.js"></script>
<script src="contenu/js/jquery.lightbox-0.5.js"></script>
