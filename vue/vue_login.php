<?php
/**
 * User: Theo.MAURON
 * Date: 19.05.2017
 * Time: 08:27
 */
ob_start();
$titre = "home";
?>
<body>
    <div id="container-login">
    <form action="index.php?action=connexion" method="POST" class="form-example">
        <div class="form Texte">
            <b><label for="Login">Login: </label></b>
            <input type="text" style="background-color: inherit; border-color:black;width:238px;"  name="Login" id="login" required>
        </div><br>
        <div class="form Texte">
            <b><label for="Login">Mot de passe: </label></b>
            <input type="password" style="background-color: inherit; border-color:black;width:238px;" name="Password" id="mdp" required>
        </div>
        <div class="form Texte">
            <input type="submit" value="Se connecter" style="">
        </div>
        </form>
        <?php if(@$_GET['erreur']==1) : ?>
            <div class="Texte erreur">
                Login ou mot de passe incorrect
            </div>
        <?php endif; ?><br>
        <div class="Texte" style="font-size:20px">
            Vous n'avez pas encore de compte ?  
            <a href="index.php?action=inscription">Inscription</a>
        </div>
    </div>
    
</body>
<?php
$contenu = ob_get_clean();
require 'vue/gabarit.php';