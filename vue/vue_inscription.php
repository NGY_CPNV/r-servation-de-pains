<?php
/**
 * User: Theo.MAURON
 * Date: 19.05.2017
 * Time: 08:27
 */
ob_start();
$titre = "home";
?>
<body>
    <div id="container-inscription">
    <form action="index.php?action=demande_inscription" method="POST" class="form-example">
        <div class="formInscr Texte">
            <b><div class="label" for="Nom">Nom :</div></b>
            <input type="text" style="background-color: inherit; border-color:black;width:238px;"  name="Nom" id="Nom" required>
        </div><br>
        <div class="formInscr Texte">
            <b><div class="label" for="Prenom">Prénom :</div></b>
            <input type="text" style="background-color: inherit; border-color:black;width:238px;"  name="Prenom" id="Prenom" required>
        </div><br>
        <div class="formInscr Texte">
            <b><div class="label" for="Email">Email :</div></b>
            <input type="text" style="background-color: inherit; border-color:black;width:238px;"  name="Email" id="Email" required>
        </div><br>
        <div class="formInscr Texte">
            <b><div class="label" for="Adresse">Adresse :</div></b>
            <input type="text" style="background-color: inherit; border-color:black;width:238px;"  name="Adresse" id="Adresse" required>
        </div><br>
        <div class="formInscr Texte">
            <b><div class="label" for="Login">Login :</div></b>
            <input type="text" style="background-color: inherit; border-color:black;width:238px;"  name="Login" id="Login" required>
        </div><br>
        <div class="formInscr Texte">
            <b><div class="label" for="Password">Mot de passe: </div></b>
            <input type="password" style="background-color: inherit; border-color:black;width:238px;" name="Password" id="Password" required>
        </div>
        <div class="formInscr Texte">
            <input type="submit" value="Demande d'inscription">
        </div>
        </form>
    </div>
    
</body>
<?php
$contenu = ob_get_clean();
require 'vue/gabarit.php';